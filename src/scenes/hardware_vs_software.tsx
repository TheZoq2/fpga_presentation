import {Circle, CubicBezier, makeScene2D, Rect, Txt, View2D} from '@motion-canvas/2d';
import {all, beginSlide, chain, createRef, linear, Reference, sequence, Vector2, waitFor} from '@motion-canvas/core';
import { baseColors, Operation, Wire } from '../electrical';
import { DummyDelay } from '../util';
import {CodeBlock, edit} from '@motion-canvas/2d/lib/components/CodeBlock';


export default makeScene2D(function* (view) {
  let code = 
  `fn add_three(a, b, c) {
    return a + b + c
  }`;
  var codeRef: Reference<CodeBlock> = createRef();
  view.add(
    <CodeBlock ref={codeRef} code={code}/>
  )
  yield* beginSlide("pre-code-transition")

  yield* beginSlide("end")

  yield* codeRef().edit(1.2)`fn add_three(a, b, c) {
      ${edit("return a + b + c",
      `load a
      load b
      load c
      add a, b -> tmp
      add tmp, c -> result
      return result`
      )}
}`
});
