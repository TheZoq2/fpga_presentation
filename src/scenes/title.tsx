
import {Circle, CubicBezier, makeScene2D, Rect, Txt, View2D} from '@motion-canvas/2d';
import {all, beginSlide, chain, createRef, linear, Reference, sequence, Vector2, waitFor} from '@motion-canvas/core';
import { baseColors, Operation, Wire } from '../electrical';


export default makeScene2D(function* (view) {
  view.add(<Txt
      fill='#ffffff'
      x={0}
      y={-50}
      scale={3}
      >
        How 2 FPGA
      </Txt>);

  view.add(<Txt
      fill='#ffffff'
      x={0}
      y={100}
      scale={1.5}
      >
        Frans Skarman
      </Txt>);

  var dummyCircle: Reference<Circle> = createRef();
  view.add(<Circle ref={dummyCircle} fill="#000000"/>)

  yield* dummyCircle().x(0, 0).to(0, 0.1)
});
