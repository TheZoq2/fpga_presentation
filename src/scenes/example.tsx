import {Circle, CubicBezier, makeScene2D, Rect, Txt, View2D} from '@motion-canvas/2d';
import {all, beginSlide, chain, createRef, createSignal, linear, Reference, sequence, SimpleVector2Signal, Vector2, waitFor} from '@motion-canvas/core';
import { baseColors, Operation, Wire } from '../electrical';


export default makeScene2D(function* (view) {
  // Create your animations here

  let pos: SimpleVector2Signal<number> = createSignal(new Vector2(0, 0));
  const inA = new Operation(pos, "A", new Vector2(-500, -300), 1, 2, true)
  const inB = new Operation(pos, "B", new Vector2(-500, 300), 1, 2, true)
  const outC = new Operation(pos, "C", new Vector2(500, 0), 1, 2, true)

  const adder = new Operation(pos, "+", new Vector2(0, 0), 2, 2)
  adder.draw(view);

  const ops = [
    inA,
    inB,
    outC,
    adder
  ];

  for (let op of ops) {
    op.draw(view)
  }

  const wireA = new Wire(inA.output_pos(0), new Vector2(200, 0), adder.input_pos(0), new Vector2(-200, 0))
  wireA.draw(view);
  const wireB = new Wire(inB.output_pos(0), new Vector2(200, 0), adder.input_pos(1), new Vector2(-200, 0))
  wireB.draw(view);

  const wireC = new Wire(adder.output_pos(0), new Vector2(200, 0), outC.input_pos(0), new Vector2(-200, 0))
  wireC.draw(view);


  const frames: Array<[string, string, [number, number]]> = [
    [baseColors[0], baseColors[1], [1,  2]],
    [baseColors[1], baseColors[2], [3,  5]],
    [baseColors[2], baseColors[3], [-2, 4]],
    [baseColors[3], baseColors[0], [4, -1]],
  ]

  yield *beginSlide("example");

  for (let [initial, to, [valA, valB]] of frames) {
    yield* chain(
      inA.textRef().fill(initial, 0).to(to, 0),
      inA.textRef().text(valA.toString(), 0),
      inB.textRef().fill(initial, 0).to(to, 0),
      inB.textRef().text(valB.toString(), 0),
      all(
        wireA.transition(initial, to),
        wireB.transition(initial, to),
      ),
      adder.transition(initial, to),
      wireC.transition(initial, to),
      outC.textRef().text((valA+valB).toString(), 0),
    );
    yield* beginSlide([valA, valB].toString())
  }
});
