import {Circle, CubicBezier, makeScene2D, Rect, Txt, View2D} from '@motion-canvas/2d';
import {all, beginSlide, chain, createRef, createSignal, linear, Reference, sequence, Vector2, waitFor} from '@motion-canvas/core';
import { baseColors, Operation, Wire } from '../electrical';
import { DummyDelay } from '../util';
import {CodeBlock, edit} from '@motion-canvas/2d/lib/components/CodeBlock';


export default makeScene2D(function* (view) {
  var rtext: Reference<Txt> = createRef();
  var gtext: Reference<Txt> = createRef();
  var btext: Reference<Txt> = createRef();

  var segmentWidth = createSignal(200)

  view.add(
    <Rect gap={10} layout>
      <Rect padding={5} radius={10} fill="#992222">
        <Txt ref={rtext} width={segmentWidth()} fill="#ffffff">Red</Txt>
      </Rect>
      <Rect padding={5} radius={10} fill="#229922">
        <Txt ref={gtext} width={segmentWidth()} fill="#ffffff">Green</Txt>
      </Rect>
      <Rect padding={5} radius={10} fill="#222299">
        <Txt ref={btext} width={segmentWidth()} fill="#ffffff">Blue</Txt>
      </Rect>
      <Rect padding={5} radius={10} fill="#992222">
        <Txt width={segmentWidth()} fill="#ffffff"></Txt>
      </Rect>
      <Rect padding={5} radius={10} fill="#229922">
        <Txt width={segmentWidth()} fill="#ffffff"></Txt>
      </Rect>
      <Rect padding={5} radius={10} fill="#222299">
        <Txt width={segmentWidth()} fill="#ffffff"></Txt>
      </Rect>
    </Rect>
  )

  yield* beginSlide("start")

  yield* all(
    yield segmentWidth(100, 1),
    rtext().width(segmentWidth(), 0).to(1000, 1),
    gtext().width(segmentWidth(), 0).to(100, 1),
    gtext().text("Green", 0).to("", 1),
    btext().width(segmentWidth(), 0).to(100, 1),
    btext().text("Blue", 0).to("", 1),
  )

  yield* beginSlide("end")
});
