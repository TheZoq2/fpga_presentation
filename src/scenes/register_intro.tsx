import { makeScene2D } from "@motion-canvas/2d";
import { all, beginSlide, chain, createSignal, delay, SimpleVector2Signal, Vector2, waitFor } from "@motion-canvas/core";
import { badColor, baseColors, Operation, Register, Wire } from "../electrical";

export default makeScene2D(function* (view: any) {
  const pos: SimpleVector2Signal<number> = createSignal(new Vector2(0, 0))

  var reg = new Register(pos, new Vector2(0, 0))
  const inIn = new Operation(pos, "in", new Vector2(-500, 0), 1, 2, true)
  const inClk = new Operation(pos, "clk", new Vector2(0, 350), 1, 2, true)
  const outOut = new Operation(pos, "out", new Vector2(500, 0), 1, 2, true)

  const wireIn = new Wire(inIn.output_pos(0), new Vector2(200, 0), reg.input_pos(0), new Vector2(-200, 0))
  const wireOut = new Wire(reg.output_pos(0), new Vector2(200, 0), outOut.input_pos(0), new Vector2(-200, 0))
  const wireClk = new Wire(inClk.north(), new Vector2(0, -100), reg.clock_pos(0), new Vector2(0, 200))



  wireIn.draw(view)
  wireOut.draw(view)

  reg.draw(view)
  inIn.draw(view)
  inClk.draw(view)
  outOut.draw(view)
  wireClk.draw(view)

  reg.textRef().text("0")

  yield* reg.botRef().fill(baseColors[1], 0)
  yield* beginSlide("start")
  yield* wireOut.transition(baseColors[0], baseColors[1])
  outOut.textRef().text("0")
  yield* beginSlide("initialTransition")

  inIn.textRef().text("1")
  yield* wireIn.transition(baseColors[0], baseColors[2]);
  yield* beginSlide("waitMoreTransitions")
  inIn.textRef().text("2"),
  yield* wireIn.transition(baseColors[2], baseColors[3]);
  inIn.textRef().text("4"),
  yield* wireIn.transition(baseColors[3], baseColors[2]);

  yield* beginSlide("waitFirstClock")
  inClk.textRef().text("1")
  yield* wireClk.transition(baseColors[0], badColor)

  yield* reg.setValue(baseColors[2], "4")
  yield* wireOut.transition(baseColors[1], baseColors[2])
  outOut.textRef().text("4")

  yield* beginSlide("moreExamples")
  inIn.textRef().text("1")
  yield* wireIn.transition(baseColors[2], baseColors[3]);
  yield* waitFor(1)
  inIn.textRef().text("2"),
  yield* wireIn.transition(baseColors[3], baseColors[1]);
  yield* waitFor(1)
  inIn.textRef().text("4"),
  yield* wireIn.transition(baseColors[1], baseColors[2]);
  yield* waitFor(1)
  inIn.textRef().text("0"),
  yield* wireIn.transition(baseColors[2], baseColors[3]);


  yield* beginSlide("waitForClockFall")
  inClk.textRef().text("0")
  yield* wireClk.transition(badColor, baseColors[0])
  inClk.textRef().text("1")
  yield* wireClk.transition(baseColors[0], badColor)

  yield* reg.setValue(baseColors[3], "0")
  yield* wireOut.transition(baseColors[2], baseColors[3])
  outOut.textRef().text("0")

  yield* beginSlide("end")
})
