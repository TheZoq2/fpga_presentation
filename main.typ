// Get polylux from the official package repository
#import "theme.typ": *
#import "highlight.typ": *
#import "@preview/diagraph:0.2.1": *

#set text(size: 25pt, font: "Noto Sans")

#show: university-theme.with(
  progress-bar: false
)

#show: highlights

#let cols(content1, content2) = {
  grid(columns: (50%, 50%), content1, content2)
}


// #show raw.where(lang: "spade"): it => [
//   #show regex("\b(entity|let|inst|end)\b") : keyword => text(weight:"bold", keyword)
//   #it
// ]

#title-slide(
  authors: ("Frans Skarman"),
  title: "How to Program Hardware",
  institution-name: "",
)

#slide()[
  #grid(columns: (50%, 50%), [
    #include "./animated_out/software_rust.typ"
  ],
  [
    #include "./animated_out/software_asm.typ"
  ])
]

#slide()[
  #set align(center)
  #only("1")[#image("./fig/chip_input_only.svg")]
  #only("2")[#image("./fig/chip_first_adder.svg")]
  #only("3")[#image("./fig/chip_second_adder.svg")]
  #only("4")[#image("./fig/chip_with_output.svg")]
]

#slide()[
  \<This is a screenshot from nextpnr\>
]

#slide()[
  #grid(columns: (35%, 30%, 30%), [
    #include "./animated_out/software_rust.typ"
  ],
  [black magic],
  [
    #only("4")[#image("./fig/chip_with_output.svg")]

  ])
]

#slide()[
  #set align(center)
  #set text(size: 32pt)

  #v(3cm)
  Some Hardware Primitives
]

#slide(title: [Boolean Logic])[
  #only("1", [#image("./fig/logic_gates_base.svg", width:85%)])
  #only("2", [#image("./fig/logic_gates_eu.svg", width:85%)])
  #only("3", [#image("./fig/logic_gates_murica.svg", width:85%)])

  #uncover("4-", ["If something isn't hard enough to understand, give it multiple names"])
]


#slide(title: [Math])[
  #only("1", [#image("./fig/math_gates_base.svg", width:85%)])
  #only("2", [#image("./fig/math_gates_scary.svg", width:85%)])
  #only("3", [#image("./fig/math_gates_with_division.svg", width:85%)])
]


#slide()[
  #set align(center)
  #only("1")[#image("./fig/mux_base.svg")]
  #only("2")[#image("./fig/mux_sel_a.svg")]
  #only("3")[#image("./fig/mux_sel_b.svg")]
]


#slide()[
  #set text(size:20pt)
  #cols([
    #include "./animated_out/alu.typ"
    ],
    [
      #set align(center)
      #only("5", image("./fig/alu_base.svg", width: 90%))
      #only("6", image("./fig/alu_separate_inputs.svg", width: 90%))
      #only("7", image("./fig/alu_sel_add.svg", width: 90%))
      #only("8", image("./fig/alu_sel_sub.svg", width: 90%))
      #only("9", image("./fig/alu_sel_mul.svg", width: 90%))
    ]
  )
]

#slide(title: [Recap so Far])[
  - Hardware is physical primitives inside a chip
  - Hardware description selects components and how to connect them
  - Programming looks similar
  - *But* Everything is done in parallel
]

#slide()[
  #set align(center)
  #set text(size: 32pt)

  #v(3cm)
  Dealing with state
]

#slide()[
  #set align(center)
  #image("./fig/the_real_world.jpg")
]

#slide()[
  #v(3cm)
  #set text(size: 20pt)
  #include "./animated_out/accumulator.typ"
]

#slide()[
  We have a very limited programming model!

  No loops, no conditional execution etc.

  State machines give complex behaviour over time
]

#slide()[
  #set align(center)
  #v(3cm)
  Every time a short pulse happens, generate a longer pulse
]

#slide(title: [In Software])[
  ```rust
  fn extend_pulse(input: bool) {
    loop {
      set_output(false)
      // Wait for pulse
      while input == 0 {}

      set_output(true)
      for i in 0..3 {
        wait_clock()
      }
      set_output(false)
    }
  }
  ```
]

#slide(title: [In Hardware])[
  #set text(size: 18pt)
  ```spade
  enum State {
    WaitPulse, Pulse{duration: int<5>}
  }
  entity extend_pulse(
    clk: clock, rst: bool, input: bool
  ) -> bool {
    reg(clk) state reset(rst: WaitPulse) =
      match (state, input) {
        (WaitPulse, false) => WaitPulse,
        (WaitPulse, true) => Pulse,
        (Pulse(3), _) => WaitPulse,
        (Pulse(d), _) => Pulse(trunc(d+1))
      };
    match state {
      Pulse => true,
      WaitPulse => false
    }
  }
  ```
]





